import { useCzechData } from "hooks/useCzechTestData"
import React from "react"
import Plot from "react-plotly.js"
import { findLineByLeastSquares } from "scripts/trend"

export default function NationWideCovidDisplay(){
    const czechCases = useCzechData()
    return(
        <div className="nationWideDisplay">
            <div className="dailyData">
                <Plot
                //@ts-ignore 
                data={
                    [
                        {
                            type: 'lines',
                            x: czechCases.map(e=>e.dateRep),
                            y: czechCases.map(e=>e.diagnosedToday),
                            marker: {color: 'red'},
                            name: 'Nahlášené případy'
                        },
                        {
                            type: 'lines',
                            x: czechCases.map(e=>e.dateRep),
                            y: czechCases.map(e=>e.deathsToday),
                            marker: {color: 'purple'},
                            name: 'Smrti'
                        },
                        {
                            type: 'lines',
                            x: czechCases.map(e=>e.dateRep),
                            y: czechCases.map(e=>e.curedToday),
                            name: 'Vyléčení'
                        }
                    ]
                }
                layout={
                    {title: "Nové případy nákazy nemocí COVID-19 v České Republice"}
                }
                config = {
                    {displayModeBar:false}
                }
                />
                {czechCases.length > 1?
                <div>
                    <span>Včera nově nakažených: {czechCases[czechCases.length-1].diagnosedToday} (<span className={czechCases[czechCases.length-1].diagnosedToday-czechCases[czechCases.length-2].diagnosedToday > 0?"casesDifference worse addPlus": "casesDifference better"}>{czechCases[czechCases.length-1].diagnosedToday-czechCases[czechCases.length-2].diagnosedToday}</span>)</span> <br/>
                    <span>Včera vyléčených: {czechCases[czechCases.length-1].curedToday} (<span className={czechCases[czechCases.length-1].curedToday-czechCases[czechCases.length-2].curedToday > 0?"casesDifference better addPlus": "casesDifference worse"}>{czechCases[czechCases.length-1].curedToday-czechCases[czechCases.length-2].curedToday}</span>)</span><br/>
                    <span>Včera zemřelých: {czechCases[czechCases.length-1].deathsToday} (<span className={czechCases[czechCases.length-1].deathsToday-czechCases[czechCases.length-2].deathsToday > 0?"casesDifference worse addPlus": "casesDifference better"}>{czechCases[czechCases.length-1].deathsToday-czechCases[czechCases.length-2].deathsToday}</span>)</span><br/>
                </div>:""}
            </div>
            <div className="dailyData">
                <Plot
                //@ts-ignore 
                data={
                    [
                        {
                            type: 'lines',
                            x: czechCases.map(e=>e.dateRep),
                            y: czechCases.map(e=>e.diagnosedTotal),
                            marker: {color: 'red'},
                            name: 'infikováno celkem'
                        },
                        {
                            type: 'lines',
                            x: czechCases.map(e=>e.dateRep),
                            y: czechCases.map(e=>e.diagnosedTotal-e.curedTotal),
                            marker: {color: 'orange'},
                            name: 'Aktivních případů'
                        },
                        {
                            type: 'lines',
                            x: czechCases.map(e=>e.dateRep),
                            y: czechCases.map(e=>e.deathsTotal),
                            marker: {color: 'purple'},
                            name: 'Smrtí'
                        },
                        {
                            type: 'lines',
                            x: czechCases.map(e=>e.dateRep),
                            y: czechCases.map(e=>e.curedTotal),
                            marker: {color: 'green'},
                            name: 'Vyléčených'
                        }
                    ]
                }
                layout={
                    {title: "Případy nákazy nemocí COVID-19 v České Republice (Kumulativní data)"}
                }
                config = {
                    {displayModeBar:false}
                }
                />
                {czechCases.length > 1?
                <div>
                    <span>Aktivních příkladů: {czechCases[czechCases.length-1].diagnosedTotal-(czechCases[czechCases.length-1].curedTotal+czechCases[czechCases.length-1].deathsTotal)}</span> <br />
                    <span>Nakaženo celkem: {czechCases[czechCases.length-1].diagnosedTotal}</span> <br/>
                    <span>Celkem vyléčených: {czechCases[czechCases.length-1].curedTotal} </span><br/>
                    <span>Celkem zemřelých: {czechCases[czechCases.length-1].deathsTotal}</span><br/>
                </div>:""}
            </div>
            {/* <div className="dailyData">
                <Plot
                //@ts-ignore 
                data={
                    [
                        {
                            type: 'lines',
                            x: czechCases.map(e=>e.dateRep),
                            y: findLineByLeastSquares(czechCases.map(e=>e.dateRep), czechCases.map(e=>e.diagnosedToday))[1].map((e,i)=>{
                                const prirustek = czechCases[i].diagnosedToday
                                return prirustek*e;
                            }),
                            marker: {color: 'red'},
                            name: 'Vývoj počtu nakažených trend'
                        },
                    ]
                }
                layout={
                    {title: "Případy nákazy nemocí COVID-19 v České Republice (Zobrazení trendu)"}
                }
                // config = {
                    // {displayModeBar:false}
                // }
                />
                {czechCases.length > 1?
                <div>
                    <span>Aktivních příkladů: {czechCases[czechCases.length-1].diagnosedToday-czechCases[czechCases.length-1].curedToday}</span> <br />
                    <span>Nakaženo celkem: {czechCases[czechCases.length-1].diagnosedTotal}</span> <br/>
                    <span>Celkem vyléčených: {czechCases[czechCases.length-1].curedTotal} </span><br/>
                    <span>Celkem zemřelých: {czechCases[czechCases.length-1].deathsTotal}</span><br/>
                </div>:""}
            </div> */}
        </div>
    
)
}
/*{
    type: 'lines',
    x: czechCases.map(e=>e.dateRep),
    y: czechCases.map(e=>(e.diagnosedTotal - e.curedTotal)),
    marker: {color: 'blue'}
}
{
    type: 'lines',
    x: czechCases.map(e=>e.dateRep),
    y: czechCases.map(e=>e.deathsTotal),
    name: 'Deaths (Cumulative)'
}
*/

/*className={czechCases[czechCases.length-1].diagnosedToday-czechCases[czechCases.length-2].diagnosedToday > 0?"casesDifference more": "casesDifference less"}*/