import { build, defineConfig } from "vite"
import ReactPlugin from "@vitejs/plugin-react"
import TsconfigPaths from "vite-tsconfig-paths"

export default defineConfig({
    plugins: [
        ReactPlugin(),
        TsconfigPaths()
    ],
    build: {
        outDir: "out",
    }

})