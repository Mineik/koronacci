import {useEffect, useState} from "react"

export function useCzechData(){
    const [covidData, updateCovidData] = useState<{dateRep: string,
            curedToday: number, curedTotal: number,
            diagnosedToday: number, diagnosedTotal: number,
            deathsToday: number, deathsTotal: number}[]>([
               {dateRep: "",
                curedToday: 0, curedTotal: 0,
                diagnosedToday: 0, diagnosedTotal: 0,
                deathsToday: 0, deathsTotal: 0}])
    //https://onemocneni-aktualne.mzcr.cz/api/v2/covid-19/nakaza.json
    //https://onemocneni-aktualne.mzcr.cz/api/v2/covid-19/nakazeni-vyleceni-umrti-testy.json
    useEffect(()=>{
        fetch("https://onemocneni-aktualne.mzcr.cz/api/v2/covid-19/nakazeni-vyleceni-umrti-testy.json").then(e=>e.json().then(data=>{
        updateCovidData(data.data.map((elem,i)=>{
            if(i > 0){
                const e2 = data.data[i-1]
                return{
                    dateRep: elem.datum,
                    curedToday: elem.kumulativni_pocet_vylecenych - e2.kumulativni_pocet_vylecenych,
                    curedTotal: elem.kumulativni_pocet_vylecenych,
                    diagnosedToday: elem.kumulativni_pocet_nakazenych - e2.kumulativni_pocet_nakazenych,
                    diagnosedTotal: elem.kumulativni_pocet_nakazenych,
                    deathsToday: elem.kumulativni_pocet_umrti - e2.kumulativni_pocet_umrti,
                    deathsTotal: elem.kumulativni_pocet_umrti
                }
            }else return{
                    dateRep: elem.datum,
                    curedToday: elem.kumulativni_pocet_vylecenych,
                    curedTotal: elem.kumulativni_pocet_vylecenych,
                    diagnosedToday: elem.kumulativni_pocet_nakazenych,
                    diagnosedTotal: elem.kumulativni_pocet_nakazenych,
                    deathsToday: elem.kumulativni_pocet_umrti,
                    deathsTotal: elem.kumulativni_pocet_umrti
            }
        }))
    }))},[])
    if(covidData.length > 0 || covidData[0].dateRep==="") return covidData
}